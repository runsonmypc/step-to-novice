import bpy
import os
import bpy_extras
import errno

# Uses the name stored in file_path.txt
# This is stored by file_select.py, which lets the user choose a .obj file.
with open('file_path.txt', 'r') as myfile:
    objfile=myfile.read().replace('\n', '')
ofile = os.path.basename(objfile)
ofile = os.path.splitext(ofile)[0]

# Import the selected obj
imported_object = bpy.ops.import_scene.obj(filepath=objfile)

# Join all objects into a single object
for ob in bpy.context.scene.objects:
    if ob.type == 'MESH':
        bpy.context.view_layer.objects.active = ob
        bpy.context.active_object.select_set(state=True)
    else:
        ob.select = False
bpy.ops.object.join()

obj_object = bpy.context.selected_objects[0]
print('Part name: ', obj_object.name)

dir_path = os.path.dirname(os.path.realpath(__file__))
savedir = os.path.dirname(os.path.dirname(dir_path))+"/output/2 vox/"
print("Save directory: ", savedir)
if not os.path.exists(os.path.dirname(savedir)):
    try:
        os.makedirs(os.path.dirname(savedir))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise


# Run voxelization script on objfile
filename = "voxelize.py"
exec(compile(open(filename).read(), filename, 'exec'))


# Create output path
blend_file_path = bpy.data.filepath
directory = os.path.dirname(blend_file_path)
target_file = savedir+ofile+'_vox.obj'

# Export voxelized .obj
bpy.ops.export_scene.obj(filepath=target_file)
