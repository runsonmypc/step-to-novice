from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename


root = Tk( )

file_name = askopenfilename(initialdir="Users/",
                           filetypes =(("Wavefront Object File", "*.obj"),("All Files","*.*")),
                           title = "Choose a obj file."
						   )

with open("file_path.txt", "w") as text_file:
    text_file.write(file_name)