import bpy
import bmesh
from math import ceil
from mathutils import Vector
from mathutils.bvhtree import BVHTree
from itertools import product


# Select active object (must be highlited in orange)
bpy.ops.object.select_all(action='SELECT')
obj = obj_object
name = obj.name


# Create an object from vertices only
def create_object( context, vertices ):
    mesh = bpy.data.meshes.new("voxel")
    obj = bpy.data.objects.new("voxel", mesh)
    context.scene.collection.objects.link(obj)
    bm = bmesh.new()

    for v in vertices:
        bm.verts.new(v)

    bm.to_mesh( mesh )
    bm.free()

    return obj

# Get object bounding as min max vectors
def boundings( obj ):
    min_x = min((Vector(b).x for b in obj.bound_box))
    max_x = max((Vector(b).x for b in obj.bound_box))
    min_y = min((Vector(b).y for b in obj.bound_box))
    max_y = max((Vector(b).y for b in obj.bound_box))
    min_z = min((Vector(b).z for b in obj.bound_box))
    max_z = max((Vector(b).z for b in obj.bound_box))
    return Vector((min_x, min_y, min_z)), Vector((max_x, max_y, max_z)) 

# Voxel
def voxelize(context, obj, size):
    # Get boundings
    min_bounds, max_bounds = boundings(obj)
    # Delta vector
    delta_bounds = max_bounds - min_bounds
    # Vertex count in all dimensions
    counts = Vector((ceil(delta_bounds.x / size), ceil(delta_bounds.y / size), ceil(delta_bounds.z / size)))
    # Reajusted starting position
    starts = min_bounds - ((counts * size) - delta_bounds) * 0.5

    # A BVHTree from the mesh
    depsgraph = context.evaluated_depsgraph_get()
    tree = BVHTree.FromObject(obj, depsgraph)

    half_size = size / 2.0
    vertices = [] 
    # Cross products of the wanted ranges
    for x, y, z in product(range(int(counts.x)+1), range(int(counts.y)+1), range(int(counts.z)+1)):
        # Corresponding position
        vert = starts + Vector((x * size, y * size, z * size))

        # Test the proximity to the mesh with the BVH tree
        result = tree.find_nearest_range(vert, half_size)
        # If close enough add the location
        if result:
            vertices.append(vert)

    # Create an mesh from the result
    return create_object(context, vertices)


# Get current object    
obj = bpy.context.object

# Define voxel size in mm with user inpute
print()
voxel_size = float(input('Enter desired voxel size (0.99 mm recommended): '))

# Add cube primitive mesh
bpy.ops.mesh.primitive_cube_add(size = voxel_size)

# Create the voxel
voxelize(bpy.context, obj, voxel_size)

print()
print("Voxel mesh created.")
print()
print("Please wait, creating voxels...")
print()

# Add cubes to mesh
ob = bpy.context.scene.objects["Cube"]
bpy.ops.object.select_all(action='DESELECT')
bpy.context.view_layer.objects.active = ob
ob.select_set(True)
ob = bpy.context.scene.objects["voxel"]
bpy.context.view_layer.objects.active = ob
ob.select_set(True)

bpy.ops.object.parent_set(type='OBJECT')
bpy.context.object.instance_type = 'VERTS'

# Make duplicates real
bpy.ops.object.duplicates_make_real()

# Delete unnecessary objects (original object and cube)
ob = bpy.context.scene.objects["Cube"]
bpy.ops.object.select_all(action='DESELECT')
bpy.context.view_layer.objects.active = ob
ob.select_set(True)
bpy.ops.object.delete()

ob = bpy.context.scene.objects[name]
bpy.ops.object.select_all(action='DESELECT')
bpy.context.view_layer.objects.active = ob
ob.select_set(True)
bpy.ops.object.delete()

ob = bpy.context.scene.objects["voxel"]
bpy.ops.object.select_all(action='DESELECT')
bpy.context.view_layer.objects.active = ob
ob.select_set(True)
bpy.ops.object.delete()
