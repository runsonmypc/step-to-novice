from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename


root = Tk( )

file_name = askopenfilename(initialdir="Users/",
                           filetypes =(("STEP file", "*.stp *.step"),("All Files","*.*")),
                           title = "Choose a step file."
						   )

with open("file_path.txt", "w") as text_file:
    text_file.write(file_name)