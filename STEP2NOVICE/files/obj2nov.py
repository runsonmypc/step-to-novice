from tkinter import Tk
from tkinter.filedialog import askopenfilename

# Progress bar display command line function for later
def printProgressBar (iteration, total, prefix = '', decimals = 1, length = 100,
fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * 
    (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%%' % (prefix, bar, percent), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()


# Prompt user for file selection
root = Tk( )
root.title("OBJ")
filepath = askopenfilename(initialdir="Users/",
                           filetypes =(("Wavefront Object File", "*.obj"),
                           ("All Files","*.*")),
                           title = "Choose a .obj file."
						   )


import os
import numpy as np
from numpy import array
import errno


# The obj file
fyle = open(filepath,"r")

# The name (without extension) of the selected file
filename = os.path.basename(os.path.splitext(filepath)[0])

# Count number of distinct objects (cubes)
o = 0
for line in fyle:
    if line[0] == "o":
        o += 1
print("number of cubes =", o)


# Generate array with size = number of cubes
vec = array([0.0] * 3)
arr = array([vec] * 8)
cube = array([arr] * o)
# Each element of cube is an array of the 8 co-ordinates that make up the cube
i = -1
j = 0
for line in open(filepath):
    vec_line = line.split(" ")
    if line[0] == "o" and line[2] == "C":
        i += 1
        j = 0
    if line[0] == "v" and line[1] == " ":
        cube[i][j] = array([float(vec_line[1]),
        float(vec_line[2]),float(vec_line[3])])
        j += 1

# cube[i][min] and cube[i][max] are opposite corners of the ith cube
# These are the only values needed to make NOVICE boxes
# ***NEGATIVE DIMENSION ERRORS MEAN THIS IS SOMEHOW NOT TRUE***
#   ->probably means the .obj cubes are inconsistently formatted

def column(matrix, i):
    return [row[i] for row in matrix]

# c1[i] is the corner opposite c2[i] for the ith cube
def corners(cube):
    c1 = []
    c2 = []
    
    for i in range(len(cube)):
        c1.append([min(column(cube[i], 0)), min(column(cube[i], 1)),
        min(column(cube[i], 2))])
        c2.append([max(column(cube[i], 0)), max(column(cube[i], 1)),
        max(column(cube[i], 2))])

    '''c1, c2 = [[] * 3] * len(cube), [[] * 3] * len(cube)
    for i in range(len(cube)):
        c1[i] = [min(column(cube[i], 0)), min(column(cube[i], 1)),
        min(column(cube[i], 2))]
        c2[i] = [max(column(cube[i], 0)), max(column(cube[i], 1)),
        max(column(cube[i], 2))]'''

    return c1, c2

c1, c2 = corners(cube)

cs = []
for i in range(len(c1)):
    cs.append([c1[i][0], c1[i][1], c1[i][2], c2[i][0], c2[i][1], c2[i][2]])


################################################################################
# Code for fixing co-ordinate accuracy problem
# Uses 'Decimal' format to limit (in)accuracy of numbers
from decimal import Decimal, getcontext



# csd is the array cs but with elements cast as decimal with 6 d.p.
# Format is x1, y1, z1, x2, y2, z2 (sorry)
csd = [[]*6] * len(cs)
for i in range(len(cs)):
    csd[i] = [Decimal(member).quantize(Decimal('1.000000')) for member in cs[i]]


# Convert back to numpy array
# NEED TO REWRITE AS LIST OPERATIONS
csd = array(csd)

xmin = min(csd[:,0])
xmax = max(csd[:,3])
ymin = min(csd[:,1])
ymax = max(csd[:,4])
zmin = min(csd[:,2])
zmax = max(csd[:,5])
print("CSD LENGTH =", len(csd[:,0]))
print("xmax =", xmax, "xmin =", xmin)
print("ymax =", ymax, "ymin =", ymin)
print("zmax =", zmax, "zmin =", zmin)
# Round side to work with grid
side = abs(csd[0][0] - csd[0][3])
side = Decimal(side).quantize(Decimal('1.000000'))
print("cube size =", side)

def gridmake(xmin, ymin, zmin):
    "Create grid arrays starting from min dimensions"
    xgrid = [xmin]
    ygrid = [ymin]
    zgrid = [zmin]
    i = 0
    print("GRID")
    print(xmin, ymin, zmin)
    print(xmax, ymax, zmax)
    while xgrid[i] <= xmax:
        xgrid.append(xgrid[i] + side)
        i += 1

    i = 0
    while ygrid[i] <= ymax:
        ygrid.append(ygrid[i] + side)
        i += 1

    i = 0
    while zgrid[i] <= zmax:
        zgrid.append(zgrid[i] + side)
        i += 1
        
    return xgrid, ygrid, zgrid


print()
print("Creating grid...")
print()
xgrid, ygrid, zgrid = gridmake(xmin, ymin, zmin)


from operator import itemgetter

def mindex(a):
    return min(enumerate(abs(a)), key=itemgetter(1))[0]

def grid(csd):
    "Go through array csd and change every number to closest match in grid"
    i = 0
    while i < len(csd):
        xidx1 = mindex(array(xgrid) - csd[:,0][i])
        xidx2 = mindex(array(xgrid) - csd[:,3][i])

        yidx1 = mindex(array(ygrid) - csd[:,1][i])
        yidx2 = mindex(array(ygrid) - csd[:,4][i])

        zidx1 = mindex(array(zgrid) - csd[:,2][i])
        zidx2 = mindex(array(zgrid) - csd[:,5][i])

        if(xgrid[xidx1] != xgrid[xidx2]
        and ygrid[yidx1] != ygrid[yidx2]
        and zgrid[zidx1] != zgrid[zidx2]):

            csd[:,0][i] = xgrid[xidx1]
            csd[:,3][i] = xgrid[xidx2]

            csd[:,1][i] = ygrid[yidx1]
            csd[:,4][i] = ygrid[yidx2]

            csd[:,2][i] = zgrid[zidx1]
            csd[:,5][i] = zgrid[zidx2]

            i += 1

        else:
            print("Bad grid cube at i =", i)
            print(xgrid[xidx1], xgrid[xidx2])
            print(ygrid[yidx1], ygrid[yidx2])
            print(zgrid[zidx1], zgrid[zidx2])
            print(csd[i])
            csd = np.delete(csd, i)
        
    return(csd)

print("Fitting cubes to grid...")
csd = grid(csd)



################################################################################
# Code for simplifying the voxelated model into minimum number of boxes

x_range = max(xgrid) - min(xgrid)
y_range = max(ygrid) - min(ygrid)
z_range = max(zgrid) - min(zgrid)

ranges = [x_range, y_range, z_range]
max_dim = np.argmax(ranges)

# Merging by creating a list with the first cube and another with the rest.
# If the cube in the first list can be merged with a cube in the second list,
# the cube in the second list is removed and the cube in the first list is
# replaced with the merged shape.
# If there is no suitable merge to be done, the lowest positioned cube in
# the second list is moved to the first list and then we check if anything
# in the second list can be merged with it and so on.

# loops until qbs has been emptied into css
# the condition for merging is matching faces
def xmerge(qbs, css):
    "Loops until qbs has been emptied into css, merging if faces along x match."
    tot = len(qbs)
    j = 0
    while len(qbs) > 0:
        i = 0
        k = 0
        printProgressBar(tot - len(qbs)+1, tot, prefix = ' Merging along x:',
        length = 50)
        while i < len(qbs):
            # First check if faces are touching
            if (abs(css[j][0] - css[j][3]) == abs(css[j][0] - qbs[i][0])
            and css[j][1] == qbs[i][1]
            and css[j][2] == qbs[i][2]
            # If true up to here then corners are touching
            # Below we check if the faces match (same size)
            and abs(css[j][1] - css[j][4]) == abs(qbs[i][1] - qbs[i][4])
            and abs(css[j][2] - css[j][5]) == abs(qbs[i][2] - qbs[i][5])):
                css[j] = [css[j][0],css[j][1],css[j][2],
                qbs[i][3],qbs[i][4],qbs[i][5]]
                del qbs[i]
                k = 1
            i += 1

        if k == 0:
            qb00 = [row[0] for row in qbs]
            idx = min(enumerate(qb00), key=itemgetter(1))[0]
            css.append(qbs[idx])
            del qbs[idx]
            j += 1

    print()
    return css

def ymerge(qbs, css):
    "Loops until qbs has been emptied into css, merging if faces along y match."
    tot = len(qbs)
    j = 0
    while len(qbs) > 0:
        i = 0
        k = 0
        printProgressBar(tot - len(qbs)+1, tot, prefix = ' Merging along y:',
        length = 50)
        while i < len(qbs):
            # First check if faces are touching
            if (abs(css[j][1] - css[j][4]) == abs(css[j][1] - qbs[i][1])
            and css[j][0] == qbs[i][0]
            and css[j][2] == qbs[i][2]
            # If true up to here then corners are touching
            # Below we check if the faces match (same size)
            and abs(css[j][0] - css[j][3]) == abs(qbs[i][0] - qbs[i][3])
            and abs(css[j][2] - css[j][5]) == abs(qbs[i][2] - qbs[i][5])):
                css[j] = [css[j][0],css[j][1],css[j][2],
                qbs[i][3],qbs[i][4],qbs[i][5]]
                del qbs[i]
                k = 1
            i += 1

        if k == 0:
            qb11 = [row[1] for row in qbs]
            idx = min(enumerate(qb11), key=itemgetter(1))[0]
            css.append(qbs[idx])
            del qbs[idx]
            j += 1

    print()
    return css

def zmerge(qbs, css):
    "Loops until qbs has been emptied into css, merging if faces along z match."
    tot = len(qbs)
    j = 0
    while len(qbs) > 0:
        i = 0
        k = 0
        printProgressBar(tot - len(qbs)+1, tot, prefix = ' Merging along z:',
        length = 50)
        while i < len(qbs):
            # First check if faces are touching
            if (abs(css[j][2] - css[j][5]) == abs(css[j][2] - qbs[i][2])
            and css[j][0] == qbs[i][0]
            and css[j][1] == qbs[i][1]
            # If true up to here then corners are touching
            # Below we check if the faces match (same size)
            and abs(css[j][0] - css[j][3]) == abs(qbs[i][0] - qbs[i][3])
            and abs(css[j][1] - css[j][4]) == abs(qbs[i][1] - qbs[i][4])):
                css[j] = [css[j][0],css[j][1],css[j][2],
                qbs[i][3],qbs[i][4],qbs[i][5]]
                del qbs[i]
                k = 1
            i += 1

        if k == 0:
            qb22 = [row[2] for row in qbs]
            idx = min(enumerate(qb22), key=itemgetter(1))[0]
            css.append(qbs[idx])
            del qbs[idx]
            j += 1

    print()
    return css

print()
print("Starting object merge procedure...")

qbs = np.delete(csd, np.argmin(csd[:,0]), axis=0)
qb0 = csd.tolist()

qb00 = [row[0] for row in qb0]
cs0 = [qb0[min(enumerate(qb00), key=itemgetter(1))[0]]]

# Remove cs0 from qb0
del qb0[np.argmin(csd[:,0])]

################################################################################
qb1 = xmerge(qb0, cs0)
qb11 = [row[1] for row in qb1]
cs1 = [qb1[min(enumerate(qb11), key=itemgetter(1))[0]]]
del qb1[min(enumerate(qb11), key=itemgetter(1))[0]]

qb2 = ymerge(qb1, cs1)
qb22 = [row[2] for row in qb2]
cs2 = [qb2[min(enumerate(qb22), key=itemgetter(1))[0]]]
del qb2[min(enumerate(qb22), key=itemgetter(1))[0]]

cs3 = zmerge(qb2, cs2)

print()
print("new object count =", len(cs3))


################################################################################
################################################################################

def writeMatFileAndDetFile(name):
    "Function for writing generic .mat and .det files"
    mat_file = open(name + ".mat", "w")
    mat_file.write("c ========================================================")
    mat_file.write("==================== \n")
    mat_file.write("c \n")
    mat_file.write("c                          MATERIAL \n")
    mat_file.write("c \n")
    mat_file.write("c      File         : " + name + ".mat \n")
    mat_file.write("c      Created for OBJ Model Translation \n")
    mat_file.write("c \n")
    mat_file.write("c ========================================================")
    mat_file.write("==================== \n")
    mat_file.write("\n")
    mat_file.write("*materials / \n")
    mat_file.write("ALUMINUM Al	/	1\n")
    mat_file.close()
    
    det_file = open(name + ".det", "w")
    det_file.write("c ========================================================")
    det_file.write("==================== \n")
    det_file.write("c \n")
    det_file.write("c                          DETECTOR \n")
    det_file.write("c \n")
    det_file.write("c      File         : " + name + ".det \n")
    det_file.write("c      Created for OBJ Model Translation \n")
    det_file.write("c \n")
    det_file.write("c ========================================================")
    det_file.write("==================== \n")
    det_file.write("\n")
    det_file.write("*materials / \n")
    det_file.write("ALUMINUM Al	/	1\n")
    det_file.close()


def writeGeoFile(name):
    "Function for writing the .geo file"
    N = open(filename + ".geo", "w")
    N.write("c ===============================================================")
    N.write("============= \n")
    N.write("c \n")
    N.write("c                          Geometry \n")
    N.write("c \n")
    N.write("c      File         : " + name + ".geo \n")
    N.write("c      OBJ Voxel Model Translation to .geo \n")
    N.write("c \n")
    N.write("c ===============================================================")
    N.write("============= \n")
    N.write("\n")
    N.write("c wsp: Structure _IDF_90000000\n")
    N.write("*design,o=3/")
    N.write("\n")
    N.write("c wsp: Region _IDF_90000001\n")
    N.write("*address,R/")
    N.write("\n")

    # For loop to write each individual cube
    for i in range(len(cs3)):
        N.write("c wsp: Element _IDF_" + str(i + 1).zfill(8) + "\n")
        N.write("*design,o=3/\n")
        # Co-ordinate format is x1, x2, y1, y2, z1, z2
        N.write("1 1 2.6989 box "
                + str(cs3[i][0]) + " "
                + str(cs3[i][3]) + " "
                + str(cs3[i][1]) + " "
                + str(cs3[i][4]) + " "
                + str(cs3[i][2]) + " "
                + str(cs3[i][5]) + "/\n")
        N.write("c fin wsp: Element _IDF_" + str(i + 1).zfill(8) + "\n")
        N.write("\n")

    N.write("c fin wsp: Region _IDF_90000001\n")
    N.write("c fin wsp: Structure _IDF_90000000\n")


# Output file path
script_dir = os.path.dirname(os.path.dirname(os.path.realpath('__file__')))
filename = script_dir + "/output/3 novice/" + filename

if not os.path.exists(os.path.dirname(filename)):
    try:
        os.makedirs(os.path.dirname(filename))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise
			
			
# Run main functions
writeMatFileAndDetFile(filename)
writeGeoFile(filename)
print("saved to: ", filename)
