# step to novice

--------------------------------------------------------------------------------
Instructions:

1. Run the first .bat and select the step file to convert when prompted
2. Run the second .bat and select the converted step file in /output/obj
3. You will have to input a resolution in the command prompt window
4. Run the third .bat and select the voxelized .obj found in /output/vox
5. The translated .geo .mat and .det will be found in /output/novice

--------------------------------------------------------------------------------

5a. The .mat defines everything as aluminum
5b. The .det is empty - detectors will have to be added manually